extends Spatial

onready var state_machine: AnimationNodeStateMachinePlayback = $AnimationTree["parameters/playback"]

func from_client_data(data: ClientData):
	$plane.primary_color = data.primary_color
	$plane.secondary_color = data.secondary_color
	$plane.player_name = data.nickname

func leave():
	state_machine.travel("Leave")

func start():
	state_machine.travel("Start")

# Delete plane after it flies away
func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Leave" or anim_name == "Start":
		queue_free()

func move_to(new_pos: Vector3, time: float):
	$PositionTween.interpolate_property(self, "transform:origin", transform.origin, new_pos, time, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	$PositionTween.start()
