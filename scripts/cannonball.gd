extends RigidBody

export(float) var delete_height = -10

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if global_transform.origin.y < delete_height:
		queue_free()
