extends Spatial

export(Vector3) var plane_offset
onready var opposite_plane_offset = plane_offset * Vector3(1, 1, -1)

export(float) var plane_move_time = 3

export(PackedScene) var plane_scene

# Called when the node enters the scene tree for the first time.
func _ready():
	# Update the planes in the lobby once at the start
	if Server.is_hosting:
		_update_planes(Server.client_data)
	else:
		_update_planes(Client.client_data)
	
	# Update every time the player list is changed
# warning-ignore:return_value_discarded
	Client.connect("players_synchronized", self, "_update_planes")

func _has_plane(player: ClientData):
	return $Planes.has_node(str(player.peer_id))

func _get_plane(player: ClientData):
	return $Planes.get_node(str(player.peer_id))

func _update_planes(players: Dictionary):
	#print("Updating planes!")
	var players_array: Array = players.values()
	
	# Find the index of the center plane
	var center_plane: int = floor(len(players) / 2.0)
	
	# Add or move planes
	for idx in range(len(players_array)):
		var player = players_array[idx]
		
		# Determine where to put the plane
		var plane_pos: Vector3 = Vector3.ZERO
		
		if idx <= center_plane:
			plane_pos = opposite_plane_offset * (center_plane - idx)
		else:
			plane_pos = plane_offset * (idx - center_plane)
		
		if _has_plane(player): # Update existing
			#print("Moving player")
			var plane_node = _get_plane(player)
			plane_node.move_to(plane_pos, plane_move_time)
		else: # Add new
			#print("Adding player")
			var plane_node: Spatial = plane_scene.instance()
			plane_node.from_client_data(player)
			plane_node.name = str(player.peer_id)
			$Planes.add_child(plane_node)
			plane_node.transform.origin = plane_pos
	
	# Remove planes
	for plane in $Planes.get_children():
		if not int(plane.name) in players:
			plane.leave()
	
	$Camera.update_zoom_level(len(players))
