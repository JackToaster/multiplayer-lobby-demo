extends Label

# Called when the node enters the scene tree for the first time.
func _ready():
# warning-ignore:return_value_discarded
	Client.connect("server_connecting", self, "_connecting")
# warning-ignore:return_value_discarded
	Client.connect("server_connection_success", self, "_connected")
# warning-ignore:return_value_discarded
	Client.connect("players_synchronized", self, "_synchronized")
# warning-ignore:return_value_discarded
	Client.connect("server_connection_failed", self, "_connection_failed")

func _connecting():
	text = "Connecting..."

func _connected():
	text = "Connected, waiting for player data..."

func _synchronized(_players):
	text = "Connected"
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Lobby.tscn")

func _connection_failed():
	text = "Failed to connect."
