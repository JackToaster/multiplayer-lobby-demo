extends Camera

export(float) var zoom_per_plane
export(float) var tween_time = 2

onready var start_position = transform.origin

func update_zoom_level(num_planes: int):
	var new_transform = start_position * (1.0 + zoom_per_plane * (num_planes - 1))
	$Tween.interpolate_property(self, "transform:origin", transform.origin, new_transform, tween_time,Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	$Tween.start()
