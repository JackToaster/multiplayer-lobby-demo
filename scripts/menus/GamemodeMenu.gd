extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Host_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/menus/HostMenu.tscn")


func _on_Join_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/menus/JoinMenu.tscn")


# TODO Singleplayer
func _on_Singlplayer_pressed():
	pass # Replace with function body.
