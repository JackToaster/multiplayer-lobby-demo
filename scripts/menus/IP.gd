extends Label

var ip_set = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_update_ip()
	Server.connect("server_started", self, "_update_ip")

func _update_ip():
	if not ip_set:
		$HTTPRequest.call_deferred("request", "http://api.ipify.org/")

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	ip_set = true
	Server.server_ip_address = body.get_string_from_utf8()
	text = "Server IP: " + Server.server_ip_address
