extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Join_button_pressed():
	var data = ClientData.new()
	data.nickname = $Panel/Menu/Nickname.text
	data.primary_color = $Panel/Menu/PrimaryColor.color
	data.secondary_color = $Panel/Menu/SecondaryColor.color
	Server.start_server()
	data.peer_id = get_tree().get_network_unique_id()
	Server.register_client_object(data)
	
	get_tree().change_scene("res://scenes/Lobby.tscn")
