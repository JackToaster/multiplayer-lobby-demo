extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_Join_button_pressed():
	var data = ClientData.new()
	data.nickname = $Panel/Menu/Nickname.text
	data.primary_color = $Panel/Menu/PrimaryColor.color
	data.secondary_color = $Panel/Menu/SecondaryColor.color
	var ip = $Panel/Menu/IP.text
	Client.connect_to_server(data, ip)

# TODO Deal with LAN game button
