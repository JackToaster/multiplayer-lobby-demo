extends MeshInstance

export(PackedScene) var cannonball: PackedScene

export(float) var y_offset

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("ui_accept"):
		fire_cannonball(rand_range(4, 10))

func fire_cannonball(vel: float):
	var new_ball: RigidBody = cannonball.instance()
	add_child(new_ball)
	new_ball.global_transform.origin = global_transform.origin + global_transform.basis.y * y_offset
	new_ball.linear_velocity = vel * global_transform.basis.y
