extends Node

var network = NetworkedMultiplayerENet.new()
var port = 31416

var self_data: ClientData
var client_data: Dictionary = {}

func _ready():
	network.connect("connection_failed", self, "_on_connection_failed")
	network.connect("connection_succeeded", self, "_on_connection_succeeded")
	network.connect("server_disconnected", self, "_on_server_disconnected")

signal server_connecting
func connect_to_server(self_client_data, ip):
	network.close_connection()
	emit_signal("server_connecting")
	self_data = self_client_data
	network.create_client(ip, port)
	get_tree().set_network_peer(network)

func disconnect_from_server():
	network.close_connection()

func _on_server_disconnected():
	print("on server disconnected")

signal server_connection_failed
func _on_connection_failed():
	emit_signal("server_connection_failed")
	print("Failed to connect")
	
signal server_connection_success
func _on_connection_succeeded():
	emit_signal("server_connection_success")
	print("Successfully connected, sending client data...")
	# Send my player info to the server
	var my_id = get_tree().get_network_unique_id()
	self_data.peer_id = my_id
	Server.rpc_id(1, "register_client", self_data.to_list())

# Get all players info from server
signal players_synchronized
remote func update_client_data(new_client_data: Dictionary):
	# Convert from list back into objects
	client_data = {}
	for client in new_client_data:
		client_data[client] = ClientData.new().from_list(new_client_data[client])
	
	emit_signal("players_synchronized", client_data)
	
	print(client_data)
