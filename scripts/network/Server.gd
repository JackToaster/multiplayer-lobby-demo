extends Node

var network = NetworkedMultiplayerENet.new()
var port = 31416
var max_players = 50

var client_data: Dictionary = {}

var server_ip_address: String

var is_hosting = false

func _ready():
	pass

signal server_started
func start_server():
	print("Starting server...")
	network.create_server(port, max_players)
	get_tree().set_network_peer(network)
	print("Server started!")
	emit_signal("server_started")
	network.connect("peer_connected", self, "_on_peer_connected")
	network.connect("peer_disconnected", self, "_on_peer_disconnected")
	is_hosting = true

func stop_server():
	network.close_connection()
	is_hosting = false

func _update_all_client_data():
	var data_as_lists = {}
	for id in client_data:
		data_as_lists[id] = client_data[id].to_list()
	Client.rpc("update_client_data", data_as_lists)
	Client.emit_signal("players_synchronized", client_data)

func _on_peer_connected(player_id):
	print("User %d connected" % player_id)

func _on_peer_disconnected(player_id):
	print("User %d disconnected" % player_id)
	
	# If the client has data, remove it and propagate the change
	if client_data.erase(player_id):
		_update_all_client_data()

# Register a client over the network
remote func register_client(client_list):
	print("Registering network client with the following data:")
	print(client_list)
	var client: ClientData = ClientData.new().from_list(client_list)
	var id = get_tree().get_rpc_sender_id()
	if client.peer_id != id:
		print("Ummm this is not supposed to happen")
		print("The client's advertised ID doesn't match their actual ID. They will be kicked.")
		network.disconnect_peer(id)
	elif len(client.nickname) > 16:
		print("Client's name is too long. Kicking them.")
		network.disconnect_peer(id)
	else:
		register_client_object(client)

# Register a client (Could be network or local)
func register_client_object(client: ClientData):
	client_data[client.peer_id] = client
	_update_all_client_data()
