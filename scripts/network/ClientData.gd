extends Object
class_name ClientData

var peer_id

var primary_color: Color
var secondary_color: Color
var nickname: String


# Hack to send data over RPCs
func to_list():
	return [peer_id, primary_color, secondary_color, nickname]

func from_list(list):
	peer_id = list[0]
	primary_color = list[1]
	secondary_color = list[2]
	nickname = list[3]
	return self
