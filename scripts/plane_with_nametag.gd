extends Spatial

export(Color) var primary_color setget set_primary, get_primary
export(Color) var secondary_color setget set_secondary, get_secondary

func set_primary(new_col: Color):
	primary_color = new_col
	update_materials()

func get_primary():
	return primary_color

func set_secondary(new_col: Color):
	secondary_color = new_col
	update_materials()

func get_secondary():
	return secondary_color

export(float, 0, 1, 0) var engine_throttle setget set_throttle, get_throttle

func set_throttle(new_value: float):
	engine_throttle = new_value
	if materials_set:
		exhaust_mat.albedo_color.a = exhaust_max_alpha * engine_throttle

func get_throttle():
	return engine_throttle

export(String) var player_name

var primary_gloss: SpatialMaterial
var secondary_gloss: SpatialMaterial
var primary_matte: SpatialMaterial
var secondary_matte: SpatialMaterial

var exhaust_mat: SpatialMaterial
var exhaust_max_alpha: float

# Hack to make the materials not update until they're set
var materials_set = false

# Called when the node enters the scene tree for the first time.
func _ready():
	# Get all of the materials that need to be recolored and duplicate them
	# This wouldn't be necessary in Godot 4 with per-instance uniforms
	primary_gloss = $wings.mesh.surface_get_material(0).duplicate()
	secondary_gloss = $wings.mesh.surface_get_material(1).duplicate()
	
	primary_matte = $engines.mesh.surface_get_material(1).duplicate()
	secondary_matte = $engines.mesh.surface_get_material(0).duplicate()
	
	exhaust_mat = $Exhaust.mesh.surface_get_material(0).duplicate()
	exhaust_max_alpha = exhaust_mat.albedo_color.a
	# Recolor materials
	materials_set = true
	update_materials()
	set_throttle(engine_throttle)
	
	# Reassign materials to meshes
	$wings.mesh = $wings.mesh.duplicate()
	$wings.mesh.surface_set_material(0, primary_gloss)
	$wings.mesh.surface_set_material(1, secondary_gloss)
	$engines.mesh = $engines.mesh.duplicate()
	$engines.mesh.surface_set_material(1, primary_matte)
	$engines.mesh.surface_set_material(0, secondary_matte)
	$fuse.mesh = $fuse.mesh.duplicate()
	$fuse.mesh.surface_set_material(0, primary_matte)
	$fuse.mesh.surface_set_material(1, secondary_matte)
	$fuse.mesh.surface_set_material(3, primary_gloss)
	$fuse.mesh.surface_set_material(4, secondary_gloss)
	$Exhaust.mesh = $Exhaust.mesh.duplicate()
	$Exhaust.mesh.surface_set_material(0, exhaust_mat)
	
	# Set the player's nametag name
	$Label/Viewport/name.text = player_name

func update_materials():
	if materials_set:
		primary_gloss.albedo_color = primary_color
		secondary_gloss.albedo_color = secondary_color
		primary_matte.albedo_color = primary_color
		secondary_matte.albedo_color = secondary_color

func set_random_colors():
	set_primary(Color(rand_range(0, 1), rand_range(0, 1), rand_range(0, 1)))
	set_secondary(Color(rand_range(0, 1), rand_range(0, 1), rand_range(0, 1)))
